<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthorBook extends Model
{
    //
    protected $table = 'author_book';

    public function book_authorBook()
    {
        return $this->belongsTo('App\Book','book_id','id');
    }

    public function author_authorBook()
    {
        return $this->belongsTo('App\Author','author_id','id');
    }
}
