<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editor extends Model
{
    //
    protected $table = 'editors';

    public function book_editor()
    {
        return $this->hasMany(Book::class);
    }
}
