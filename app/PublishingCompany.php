<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublishingCompany extends Model
{
    //
    protected $table = 'publishing_companys';

    public function book_publish_company()
    {
        return $this->hasMany(Book::class);
    }
}
