<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    //
    protected $table = 'authors';

    public function book_author()
    {
        return $this->belongsToMany(Book::class);
    }

    public function authorBook_author()
    {
        return $this->hasOne('App\AuthorBook'.'author_id','id');
    }
}
