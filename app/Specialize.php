<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialize extends Model
{
    //
    protected $table = 'specializes';

    public function book_specialize()
    {
        return $this->hasMany(Book::class);
    }
}
