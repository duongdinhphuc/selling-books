<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    protected $table = 'books';

    public function publishing_company()
    {
        return $this->belongsTo(PublishingCompany::class);
    }

    public function specialize()
    {
        return $this->belongsTo(Specialize::class);
    }

    public function author()
    {
        return $this->belongsToMany(Author::class);
    }

    public function editor()
    {
        return $this->belongsTo(Editor::class);
    }

    public function authorBook_book()
    {
        return $this->hasMany('App\AuthorBook','book_id','id');
    }
}
