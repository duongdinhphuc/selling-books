<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    $router->resource('/publishing-companys', PublishingCompanyAdminController::class);
    $router->resource('/specializes', SpecializeAdminController::class);
    $router->resource('/authors',AuthorAdminController::class);
    $router->resource('/books', BookAdminController::class);
    $router->resource('/editors', EditorAdminController::class);

});
