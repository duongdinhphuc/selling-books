<?php

namespace App\Admin\Controllers;

use App\Editor;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Box;
use Illuminate\Support\Facades\DB;

class EditorAdminController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Editor);

        $grid->id('ID')->sortable();
//        $grid->editor_name('Editor Name')->display(function ($editor){
//            return title_case($editor);
//        });
        $grid->editor_name('Editor Name')->expand(function ($model) {

            $editors = $model->book_editor()->take(10)->get()->map(function ($editor) {
                return $editor->only(['book_code','book_name','date_created']);
            });

            return new \Encore\Admin\Widgets\Table(['Code','Name','Date Created'], $editors->toArray());
        });
        $grid->gender('Gender')->display(function ($gender){
            if ($gender == "m")
            {
                return "Male";
            }
            elseif($gender == "f")
            {
                return "Female";
            }
            else
            {
                return "Unknow";
            }
        });
//        $grid->status('Status')->display(function ($status){
//            if ($status == 1)
//            {
//                return "Enable";
//            }
//            else
//            {
//                return "Disable";
//            }
//        });
//        $grid->deleted_at('Deleted at');
//        $grid->created_at('Created at');
//        $grid->updated_at('Updated at');

        //header
        $grid->header(function ($query) {

            $gender = $query->select(DB::raw('count(gender) as count, gender'))
                ->groupBy('gender')->get()->pluck('count', 'gender')->toArray();

            $doughnut = view('admin.chart.gender', compact('gender'));

            return new Box('Gender Ratio', $doughnut);
        });
        //footer
        $grid->footer(function ($query) {

            // Query the total amount of the order with the paid status
            $data = $query->count('id');

            return "<div class='badge' style='padding: 10px;'>Total Editor ： $data</div>";
        });

//        $grid->deleted_at('Deleted at');
//        $grid->created_at('Created at');
//        $grid->updated_at('Updated at');

        $grid->filter(function ($filter){
            $filter->disableIdFilter();
            $filter->expand();

            $filter->like('editor_name','Editor Name');
//            $filter->in('gender')->radio([
//                'f' => 'Female',
//                'm' => 'Male',
//                'n' => 'Unknow'
//            ]);
            //$filter->in('status','Status')->radio(['1' => 'Enable','0' => 'Disable']);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Editor::findOrFail($id));

        $show->id('ID');
        $show->editor_name('Editor Name')->as(function ($editor){
            return title_case($editor);
        });
        $show->column('Alias')->as(function (){
           return str_slug($this->editor_name);
        });
        $show->gender('Gender')->as(function ($gender){
            if($gender == 'f')
            {
                return "Female";
            }
            elseif($gender == 'm')
            {
                return "Male";
            }
            else
            {
                return "Unknow";
            }
        });
//        $show->status('Status')->as(function ($status){
//            if ($status == 1)
//            {
//                return "Enable";
//            }
//            else
//            {
//                return "Disable";
//            }
//        });
        $show->deleted_at('Deleted at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Editor);

        $form->text('editor_name', 'Editor Name')->help('For example: Dương Đình Phúc...')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $gender = [
            'm' => 'Male',
            'f' => 'Female',
            'n' => 'Unknow'
        ];
        $form->select('gender','Gender')->options($gender)->rules('required',[
            'required' => 'You have not entered information'
        ]);
        //$form->switch('status', 'Status');

        //disable tool
        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();
        return $form;
    }
}
