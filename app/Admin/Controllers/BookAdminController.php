<?php

namespace App\Admin\Controllers;

use App\Author;
use App\AuthorBook;
use App\Book;
use App\Editor;
use App\Http\Controllers\Controller;
use App\PublishingCompany;
use App\Specialize;
use Doctrine\DBAL\Schema\Table;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Box;
use function foo\func;
use Illuminate\Support\Facades\DB;

class BookAdminController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Book);

        $grid->id('ID')->sortable();
        $grid->model()->orderBy('id', 'desc');
        $grid->book_code('Book Code');
        $grid->book_name('Book Name');
        $grid->description('Description');
//        $grid->description()->display(function($des) {
//            return str_limit($des, 5, '...');
//        });
        $grid->column('specialize.specialize_name','Specialize Name');
        $grid->column('publishing_company.publisher_name','Publishing Company Name');
        $grid->column('editor.editor_name','Editor Name');
        $grid->author()->display(function ($authors){
            $authors = array_map(function ($authors) {
                return "<span class='label label-success'>{$authors['author_name']}</span>";
            }, $authors);
            return join(' ', $authors);
        });
        $grid->date_created('Date Created');
        $grid->rate('Rate')->sortable();
        $grid->status('Status')->display(function ($status){
            if ($status == 1)
            {
                return "Enable";
            }
            else
            {
                return "Disable";
            }
        });
        $grid->footer(function ($query) {

            // Query the total amount of the order with the paid status
            $data = $query->where('status', 1)->count('id');

            return "<div class='badge' style='padding: 10px;'>Total Book ： $data</div>";
        });
//        $grid->deleted_at('Deleted at');
//        $grid->created_at('Created at');
//        $grid->updated_at('Updated at');

        //disable tool
        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
//            $actions->disableEdit();
//            $actions->disableDelete();
        });

        $grid->filter(function ($filter){
           $filter->expand();
           $filter->disableIdFilter();

           $filter->column(1/2, function ($filter){
               $filter->equal('book_code','Book Code');
               $filter->like('book_name','Book Name');
               $filter->equal('specialize_id','Specialize Name')->select(Specialize::all()->pluck('specialize_name','id'));
               $filter->equal('publishing_company_id','Publishing Company Name')->select(PublishingCompany::all()->pluck('publisher_name','id'));
//               $author_id = AuthorBook::all()->pluck('author_id.','author_id');
//               $filter->equal('authorBook_book.author_id')->select($author_id);
           });

            $filter->column(1/2, function ($filter){
                $filter->between('date_created','Date Created')->date();
                $filter->equal('rate','Rate');
                $filter->in('status','Status')->radio(['1' => 'Enable','0' => 'Disable']);
            });
            //scope
            $filter->scope('rate', 'Rate > 4.0')->where('rate','>', 4.0);
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Book::findOrFail($id));

        $show->id('ID');
        $show->book_code('Book Code');
        $show->book_name('Book Name');
        $show->column('Alias')->as(function (){
           return str_slug($this->book_name);
        });
        $show->description('Description');
        $show->specialize_id('Specialize Name')->as(function ($id){
            $specialize = DB::table('specializes')->select('specialize_name')
                ->where('status',1)->where('id', $id)->first();
            return $specialize->specialize_name;
        });
        $show->publishing_company_id('Publishing Company Name')->as(function ($id){
            $publishing_company = DB::table('publishing_companys')->select('publisher_name')
                ->where('status',1)->where('id',$id)->first();
            return $publishing_company->publisher_name;
        });
        $show->editor_id('Editor Name')->as(function ($id){
            $editor = DB::table('editors')->select('editor_name')
                ->where('id', $id)->first();
            return $editor->editor_name;
        });
        $show->author_id('Author Name')->as(function ($id){
            $author = DB::table('author_book')->select('author_id')->get();
            return $author;
        });
        $show->date_created('Date Created');
        $show->rate('Rate')->badge();
        $show->status('Status')->as(function ($status){
            if ($status == 1)
            {
                return "Enable";
            }
            else
            {
                return "Disable";
            }
        });
        $show->deleted_at('Deleted at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Book);

        $form->text('book_code', 'Book Code')->help('For example: S01...')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->text('book_name', 'Book Name')->help('For example: Dế mèn phiêu lưu ký...')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->textarea('description', 'Description');
        $form->select('specialize_id', 'Specialize Name')->options(Specialize::all()->pluck('specialize_name','id'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->select('editor_id','Editor Name')->options(Editor::all()->pluck('editor_name','id'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->multipleSelect('author')->options(Author::all()->pluck('author_name','id'))->rules('required|max:5',[
            'required' => 'You have not entered information',
            'max' => 'Maximum 5 authors selected'
        ]);
        $form->select('publishing_company_id', 'Publishing Company Name')->options(PublishingCompany::all()->pluck('publisher_name','id'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->date('date_created', 'Date Created')->default(date('Y-m-d'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->decimal('rate', 'Rate');
        $form->switch('status', 'Status')->default(1)->help('Default value is On(Enable)');

        //disable tool
        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();

        return $form;
    }
}
