<?php

namespace App\Admin\Controllers;

use App\Specialize;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SpecializeAdminController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Specialize);

        $grid->id('ID')->sortable();
//        $grid->specialize_name('Specialize Name')->display(function ($specialize){
//            return ucfirst($specialize);
//        });
        $grid->specialize_name('Specialize Name')->expand(function ($model) {

            $specializes = $model->book_specialize()->take(10)->get()->map(function ($specialize) {
                return $specialize->only(['book_code','book_name']);
            });

            return new \Encore\Admin\Widgets\Table(['Code','Name'], $specializes->toArray());
        });
        $grid->status('Status')->display(function ($status){
            if ($status == 1)
            {
                return "Enable";
            }
            else
            {
                return "Disable";
            }
        });
        //footer
        $grid->footer(function ($query) {

            // Query the total amount of the order with the paid status
            $data = $query->where('status', 1)->count('id');

            return "<div class='badge' style='padding: 10px;'>Total Specialize ： $data</div>";
        });
//        $grid->deleted_at('Deleted at');
//        $grid->created_at('Created at');
//        $grid->updated_at('Updated at');

        $grid->filter(function ($filter){
            $filter->disableIdFilter();
            $filter->expand();

            $filter->like('specialize_name','Specialize Name');
            $filter->in('status','Status')->radio(['1' => 'Enable','0' => 'Disable']);
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Specialize::findOrFail($id));

        $show->id('ID');
        $show->specialize_name('Specialize Name')->as(function ($specialize){
            return ucfirst($specialize);
        });
        $show->column('Alias')->as(function (){
            return str_slug($this->specialize_name);
        });
        $show->status('Status')->as(function ($status){
            if ($status == 1)
            {
                return "Enable";
            }
            else
            {
                return "Disable";
            }
        });
        $show->deleted_at('Deleted at');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Specialize);

        $form->text('specialize_name', 'Specialize Name')->help('For example: Công nghệ, Khoa học...')->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->switch('status', 'Status')->default(1)->help('Default value is On(Enable)');

        //disable tool
        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();
        return $form;
    }
}
