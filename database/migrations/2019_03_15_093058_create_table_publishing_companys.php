<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePublishingCompanys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publishing_companys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('publisher_code', 100)->nullable();
            $table->string('publisher_name', 100)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->index(['publisher_code','publisher_name','status']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publishing_companys');
    }
}
