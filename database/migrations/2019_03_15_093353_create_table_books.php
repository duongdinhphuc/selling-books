<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('book_code',100)->nullable();
            $table->string('book_name',100)->nullable();
            $table->text('description')->nullable();
            $table->integer('specialize_id')->unsigned()->nullable();
            $table->integer('editor_id')->unsigned()->nullable();
            $table->integer('author_id')->unsigned()->nullable();
            $table->integer('publishing_company_id')->unsigned()->nullable();
            $table->date('date_created')->nullable();
            $table->float('rate')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->foreign('specialize_id')->references('id')->on('specializes');
            $table->foreign('publishing_company_id')->references('id')->on('publishing_companys');
            $table->foreign('author_id')->references('id')->on('authors');
            $table->foreign('editor_id')->references('id')->on('editors');
            $table->index(['specialize_id','publishing_company_id']);
            $table->index(['date_created','author_id','editor_id']);
            $table->index(['book_code','book_name','rate','status']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
